# Why Frank Ocean's Blonde is the best album of all time
![Blonde album cover](https://www.sleek-mag.com/wp-content/uploads/2016/08/AlbumCovers_Blonde-1200x1200.jpg)

> Wishing you godspeed, glory
> There will be mountains you won't move
> Still, I'll always be there for you

---

Certain albums appear at the right time for the right people. For me that album was *Blonde* and the time was 2016, right during my junior year of high school. It's a **beautiful** collection of songs about love and loss, with a meaning that's evolved for me over time.

## Top Songs
1. Self Control
2. Godspeed
3. Nights
4. Solo
5. Nikes

## Moods
- Smooth
- Electronic
- Dark
- Romantic

## Other Frank Ocean Projects

| Album | Year |
| --- | ------ |
| Channel Orange | 2012 |
| Endless | 2016 |
| Moon River | 2017 |
| In My Room | 2019 |
